import argparse
import cv2
import random

# Parse the command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("input_video", help="path to the input video file")
parser.add_argument("num_snapshots", type=int, help="number of snapshots to take from the video")
args = parser.parse_args()

# Open the video file
video = cv2.VideoCapture(args.input_video)

# Check if the video was opened successfully
if not video.isOpened():
  print("Error opening video file")
  sys.exit()

# Set the number of snapshots to take
num_snapshots = args.num_snapshots

# Take the snapshots
for i in range(num_snapshots):
  # Choose a random time to take the snapshot
  video.set(cv2.CAP_PROP_POS_MSEC, random.randint(0, int(video.get(cv2.CAP_PROP_FRAME_COUNT))))
  success, image = video.read()
  if success:
    cv2.imwrite("snapshot_{}.jpg".format(i), image)

# Close the video file
video.release()