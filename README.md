# RandomShot

Generate random snapshots from a video file.

To run it, you gotta do `python3 randomshot.py <input video> <snapshot amount>`